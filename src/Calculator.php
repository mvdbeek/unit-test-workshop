<?php

namespace App;

class Calculator
{
    public function add(int $x, int $y) : int
    {
        return  $x + $y;
    }
}